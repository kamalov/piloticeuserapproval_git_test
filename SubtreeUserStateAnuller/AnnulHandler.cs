﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;
using Ascon.Pilot.SDK;
using IDataObject = Ascon.Pilot.SDK.IDataObject;

namespace SubtreeUserStateAnuller
{
    [Export(typeof(IObjectChangeProcessor))]
    public class AnnulHandler : IObjectChangeProcessor
    {
        public const string Xaml = "<TextBlock Foreground=\"Red\" FontSize=\"20\">АННУЛИРОВАНО</TextBlock>";

        private readonly string _annuledUserStateId;
        private int _verNumCount;

        [ImportingConstructor]
        public AnnulHandler(IObjectsRepository repository)
        {
            var userStates = repository.GetUserStates();
            var annuledUserState = userStates.FirstOrDefault(x => x.Name == Const.AnnuledUserState);
            if (annuledUserState != null)
                _annuledUserStateId = annuledUserState.Id.ToString();

        }

        public bool ProcessChanges(IEnumerable<DataObjectChange> changes, IObjectModifier modifier)
        {
            if (string.IsNullOrEmpty(_annuledUserStateId))
                return true;

            foreach (var change in changes)
            {
                foreach (var attribute in change.New.Attributes)
                {
                    if (Equals(attribute.Value.ToString(), _annuledUserStateId))
                    {
                        object oldValue = null;
                        change.Old?.Attributes.TryGetValue(attribute.Key, out oldValue);
                        _verNumCount = change.Old.PreviousFileSnapshots.Count;
                        MessageBox.Show("Количество версий: " + _verNumCount.ToString());

                        if (!Equals(oldValue, _annuledUserStateId))
                            AddGraphicLayer(change.New, modifier);
                    }
                }
            }

            return true;
        }

        private void AddGraphicLayer(IDataObject dataObject, IObjectModifier modifier)
        {
            var elementId = Guid.NewGuid();
            var builder = modifier.Edit(dataObject);
            using (var textBlocksStream = new MemoryStream())
            using (var writer = new StreamWriter(textBlocksStream))
            {
                writer.Write(Xaml);
                writer.Flush();

                var name = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + elementId;
                var element = new GraphicLayerElement(
                    elementId,
                    Guid.NewGuid(),
                    0,
                    0,
                    new Point(5, 5),
                    -45,
                    0,
                    0,
                    VerticalAlignment.Top,
                    HorizontalAlignment.Left,
                    GraphicLayerElementConstants.XAML,
                    false);

                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(name, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
                builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId, textBlocksStream, DateTime.Now, DateTime.Now, DateTime.Now);
            }
        }
    }
}
