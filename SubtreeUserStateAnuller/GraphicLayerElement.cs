﻿using System;
using System.Windows;
using Ascon.Pilot.SDK;

namespace SubtreeUserStateAnuller
{
    [Serializable]
    public class GraphicLayerElement : IGraphicLayerElement
    {
        public GraphicLayerElement()
        {
        }

        public GraphicLayerElement(Guid elementId, Guid contentId, double offsetX, double offsetY, Point scale, double angle, int positionId, int pageNumber, VerticalAlignment verticalAlignment, HorizontalAlignment horizontalAlignment, string contentType, bool isFloating)
        {
            ElementId = elementId;
            ContentId = contentId;
            OffsetX = offsetX;
            OffsetY = offsetY;
            Scale = scale;
            Angle = angle;
            PositionId = positionId;
            PageNumber = pageNumber;
            VerticalAlignment = verticalAlignment;
            HorizontalAlignment = horizontalAlignment;
            ContentType = contentType;
            IsFloating = isFloating;
        }

        public Guid ElementId { get; set; }
        public Guid ContentId { get; set; }
        public double OffsetX { get; set; }
        public double OffsetY { get; set; }
        public Point Scale { get; set; }
        public double Angle { get; set; }
        public int PositionId { get; set; }
        public int PageNumber { get; set; }
        public VerticalAlignment VerticalAlignment { get; set; }
        public HorizontalAlignment HorizontalAlignment { get; set; }
        public string ContentType { get; set; }
        public bool IsFloating { get; set; }
    }
}
