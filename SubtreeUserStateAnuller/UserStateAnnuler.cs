﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Extensions;

namespace SubtreeUserStateAnuller
{
    public class UserStateAnnuler
    {
        private readonly IObjectsRepository _repository;
        private readonly IObjectModifier _objectModifier;
        private readonly Dictionary<int, string> _supportingTypes;
        private readonly IPerson _currentPerson;
        private Guid _annuledUserStateId;
        private bool _hasAnyChanges;
        
        public UserStateAnnuler(IObjectsRepository repository, IObjectModifier objectModifier)
        {
            _repository = repository;
            _objectModifier = objectModifier;
            _supportingTypes = new Dictionary<int, string>();

            var annuledUserState = _repository.GetUserStates().FirstOrDefault(s => s.Name.Equals(Const.AnnuledUserState));
            if (annuledUserState == null)
                throw new ExpectedExtensionException(nameof(SubtreeUserStateAnuller), $"Cannot find \"{Const.AnnuledUserState}\" UserState");
            _annuledUserStateId = annuledUserState.Id;

            _currentPerson = _repository.GetCurrentPerson();

            FillTypesWithUserState();
        }

        public async void AnnulSubtree(IEnumerable<Guid> ids)
        {
            await AnnulRecursive(ids);

            if (_hasAnyChanges)
                _objectModifier.Apply();
        }

        private async Task AnnulRecursive(IEnumerable<Guid> ids)
        {
            var annuledObjects = await _repository.AsyncMethods().GetObjectsAsync(ids, AnnulObject, CancellationToken.None);
            foreach (var obj in annuledObjects)
            {
                if (obj.Children.Any())
                    await AnnulRecursive(obj.Children);
            }
        }

        private IDataObject AnnulObject(IDataObject obj)
        {
            string attributeName;
            if (!_supportingTypes.TryGetValue(obj.Type.Id, out attributeName))
                return obj;

            object attributeValue;
            if (obj.Attributes.TryGetValue(attributeName, out attributeValue) && (Guid?)attributeValue == _annuledUserStateId)
                return obj;

            var accessLevel = _repository.GetCurrentAccess(obj.Id, _currentPerson.Id);
            if ((accessLevel & AccessLevel.Edit) == AccessLevel.Edit)
            {
                var builder = _objectModifier.Edit(obj);
                builder.SetAttribute(attributeName, _annuledUserStateId);
                _hasAnyChanges = true;
            }

            return obj;
        }

        private void FillTypesWithUserState()
        {
            foreach (var type in _repository.GetTypes())
            {
                var userStateAttr = type.Attributes.FirstOrDefault(a => a.IsUserStateType() && a.GetUserStates().Contains(_annuledUserStateId));
                if (userStateAttr != null)
                    _supportingTypes.Add(type.Id, userStateAttr.Name);
            }
        }
    }
}