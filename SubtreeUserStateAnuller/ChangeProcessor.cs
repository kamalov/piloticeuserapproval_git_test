﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Ascon.Pilot.SDK;

namespace SubtreeUserStateAnuller
{
    [Export(typeof(IObjectChangeProcessor))]
    public class ChangeProcessor : IObjectChangeProcessor
    {
        public bool ProcessChanges(IEnumerable<DataObjectChange> changes, IObjectModifier modifier)
        {
            //проверяем первое изменение
            var change = changes.First();

            //получаем замечания из изменения
            var newAnnotations = change.New.ActualFileSnapshot.Files.Select(f => f.Name).Where(f => f.StartsWith("Annotation_"));
            var oldAnnotations = change.Old.ActualFileSnapshot.Files.Select(f => f.Name).Where(f => f.StartsWith("Annotation_"));

            //если для объекта добавлено замечание - отменяем изменения, иначе применяем
            if (newAnnotations.Except(oldAnnotations).Any())
                return false;

            return true;
        }
    }
}
